//
//  Director.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation


class Director {
    var name: String!
    var biography: String!

    convenience init(json: [String: AnyObject]) {
        self.init()
        self.name = json["name"] as! String?
        self.biography = json["biography"] as! String
    }

}
