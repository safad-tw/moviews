//
//  RootWireframeProtocol.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation
import UIKit

protocol RootWireframeProtocol{
    func showRootViewController(_ viewController: UIViewController, inWindow: UIWindow)
}
