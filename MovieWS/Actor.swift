//
//  Actor.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

class Actor {

    var screeName: String!
    var name: String!
    var biography: String!

    convenience init(json: [String: AnyObject]) {
        self.init()
        self.screeName = json["screenName"] as! String
        self.name = json["name"] as! String?
        self.biography = json["biography"] as! String
    }

}
