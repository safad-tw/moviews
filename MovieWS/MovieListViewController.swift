//
//  ViewController.swift
//  MovieWS
//
//  Created by safadmoh on 1/10/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {

    let cellIdentifier = "MovieListTableViewCell"
    var presenter: ListPresenter!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//MARK:- UITableview Datasource
extension MovieListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfFilms()
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MovieListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MovieListTableViewCell
        cell.setFields(presenter.filmAt(indexPath.row))
        return cell
    }
}


//MARK:- UITableview Delegate
extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectFilmAt(indexPath.row)
    }
}

