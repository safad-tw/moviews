//
//  ListWireframeProtocol.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

protocol ListWireframeProtocol {
    func goToMovieDetail(with film: Film)
}

