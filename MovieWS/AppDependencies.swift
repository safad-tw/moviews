
//
//  AppDepencies.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation
import UIKit

class AppDependencies{
    
    let listWireframe:ListWireframe!
    
    init() {
        listWireframe = ListWireframe()
        configureDependencies()
    }
    
    func installRootViewControllerIntoWindow(_ window: UIWindow) {
        listWireframe.presentListInterfaceFromWindow(window)
    }
    
    func configureDependencies(){
        let rootWireframe = RootWireframe()

        var listInteractor:ListInteractorProtocol = ListInteractor()
        let filmService:FilmServiceProtocol = FilmService()
        
        listInteractor.service = filmService
        
        let listPresenter = ListPresenter(listInteractor,wireFrame: listWireframe)
        let detailPresenter = DetailPresenter()
        
        listWireframe.rootWireframe = rootWireframe
        listWireframe.listPresenter = listPresenter
        listWireframe.detailPresenter = detailPresenter    
    }
    
    
    
}
