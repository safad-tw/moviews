//
//  MovieService.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

class FilmService:FilmServiceProtocol{
    
    func getFilms() -> [Film]{
        var films = [Film]()
        let filmJsonData =  loadDataFromJSON(fileName: "movies")!

        for film in filmJsonData{
            films.append(Film(json:film as! [String : AnyObject]))
        }
        return films
    }

}
