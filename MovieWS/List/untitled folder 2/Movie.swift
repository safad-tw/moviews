//
//  File.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

class Film {

    var languages: [String]?
    var releaseDate: Date?
    var actors = [Actor]()
    var name: String?
    var rating: Float?
    var director: Director?

    convenience init(json: [String: AnyObject]) {
        self.init()
        self.languages = json["languages"] as! [String]!
        self.releaseDate = Date(timeIntervalSince1970: json["releaseDate"] as! TimeInterval)
        self.name = json["name"] as! String?
        self.rating = json["rating"] as! Float?

        let cast: [AnyObject] = json["cast"] as! [AnyObject]
        for actor in cast {
            actors.append(Actor(json: actor as! [String: AnyObject]))
        }

        self.director = Director(json: json["director"] as! [String: AnyObject])
    }

}

