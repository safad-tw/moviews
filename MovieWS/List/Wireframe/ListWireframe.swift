//
//  ListWireframe.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import UIKit



let MovieListViewControllerIdentifier = "MovieListViewController"
let MovieDetailViewControllerIdentifier = "MovieDetailViewController"

class ListWireframe: ListWireframeProtocol {

    var listPresenter: ListPresenter?
    var detailPresenter: DetailPresenter?
    var rootWireframe: RootWireframeProtocol?
    var listViewController: MovieListViewController?

    func presentListInterfaceFromWindow(_ window: UIWindow) {
        let viewController = movieListViewControllerFromStoryboard()
        viewController.presenter = listPresenter
        listViewController = viewController
        rootWireframe?.showRootViewController(viewController, inWindow: window)
    }

    func goToMovieDetail(with film: Film) {
        let movieDetailVC = movieDetailViewControllerFromStoryboard()
        detailPresenter?.selectedFilm = film
        movieDetailVC.presenter = detailPresenter
        listViewController?.navigationController?.pushViewController(movieDetailVC, animated: true)
    }


    func movieDetailViewControllerFromStoryboard() -> MovieDetailViewController {
        let storyboard = mainStoryboard()
        let movieDetailViewController = storyboard.instantiateViewController(withIdentifier: MovieDetailViewControllerIdentifier) as! MovieDetailViewController
        return movieDetailViewController
    }

    func movieListViewControllerFromStoryboard() -> MovieListViewController {
        let storyboard = mainStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: MovieListViewControllerIdentifier) as! MovieListViewController
        return viewController
    }

    func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

}
