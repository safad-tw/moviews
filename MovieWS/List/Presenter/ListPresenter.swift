//
//  ListPresenter.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation


class ListPresenter {

    var interactor: ListInteractorProtocol!
    var wireFrame: ListWireframeProtocol!
    var films: [Film]!

    convenience init(_ listInteractor: ListInteractorProtocol, wireFrame: ListWireframeProtocol) {
        self.init()
        self.interactor = listInteractor
        self.wireFrame = wireFrame
        films = interactor.getAllFilms()
    }

    func numberOfFilms() -> Int {
        return films.count
    }

    func filmAt(_ index: Int) -> Film {
        return films[index]
    }

    func didSelectFilmAt(_ index: Int) {
        wireFrame.goToMovieDetail(with: films[index])
    }


}

