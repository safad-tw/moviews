//
//  MovieListTableViewCell.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import UIKit

class MovieListTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setFields(_ film: Film) {
        nameLabel.text = film.name
        ratingLabel.text = film.rating?.description
        dateLabel.text = film.releaseDate?.toString()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
