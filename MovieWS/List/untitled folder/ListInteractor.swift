//
//  File.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation




class ListInteractor: ListInteractorProtocol {

    var service: FilmServiceProtocol!

    func getAllFilms() -> [Film] {
        return service.getFilms()
    }


}
