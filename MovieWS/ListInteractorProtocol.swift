//
//  ListInteractorProtocol.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation


protocol ListInteractorProtocol {
    var service: FilmServiceProtocol! { get set }
    func getAllFilms() -> [Film]
}
