//
//  DetailPresenter.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

class DetailPresenter{

    var selectedFilm:Film!
    
    func getDirectorName() -> String{
        return (self.selectedFilm.director?.name)!
    }
    
    func getActorsName() -> String{
        var names = [String]()
        for actor in self.selectedFilm.actors {
            names.append(actor.name)
        }
        return names.joined(separator: ", ")
    }
    
    func getActorsScreenName() -> String{
        var screenNames = [String]()
        for actor in self.selectedFilm.actors {
            screenNames.append(actor.screeName)
        }
        return screenNames.joined(separator: ", ")
    }
    
}
