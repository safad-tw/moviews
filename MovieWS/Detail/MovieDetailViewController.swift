//
//  MovieDetailViewController.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var directorNameLabel: UILabel!
    @IBOutlet weak var actorScreenNameLabel: UILabel!
    @IBOutlet weak var actorsNameLabel: UILabel!

    var presenter: DetailPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.directorNameLabel.text = presenter.getDirectorName()
        self.actorsNameLabel.text = presenter.getActorsName()
        self.actorScreenNameLabel.text = presenter.getActorsScreenName()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
