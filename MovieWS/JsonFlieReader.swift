//
//  JsonFlieReader.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation

func loadDataFromJSON(fileName name: String) -> [AnyObject]? {
    if let path = Bundle.main.path(forResource: name, ofType: "json") {
        if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)) {
            return try! JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [AnyObject]
        }
    }
    return nil
}
