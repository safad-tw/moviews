//
//  ActorSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class ActorSpec: QuickSpec {
    
    override func spec() {
        
        it("should initialze with json data"){
            
            let actorData:[String:AnyObject] = [
                "nominated" : 1 as AnyObject,
                "name" : "Bryan Cranston" as AnyObject,
                "screenName" : "Jack Donnell" as AnyObject,
                "biography" : "Bryan Lee Cranston is an American actor, voice actor, writer and director." as AnyObject
            ]
            
            let actor = Actor(json:actorData)
            
            expect(actor).notTo(beNil())
            expect(actor.name).to(equal("Bryan Cranston"))
            expect(actor.screeName).to(equal("Jack Donnell"))
            
           
            
        }
    }

}
