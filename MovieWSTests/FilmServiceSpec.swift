//
//  FilmServiceSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation


import Quick
import Nimble
@testable import MovieWS

class FilmServiceSpec: QuickSpec {
    
    override func spec() {
        
        it("Should return movies"){
            let filmService = FilmService()
            let films = filmService.getFilms()
            expect(films).notTo(beNil())
        }
        
        
    }
    
    
    
}
