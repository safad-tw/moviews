//
//  Date+ExtensionSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class Date_ExtensionSpec: QuickSpec {
    
    override func spec() {
        
        it("should return date in string"){
            let date =  Date(timeIntervalSince1970: 23335)
            expect(date.toString()).to(equal("Jan,01 1970"))
        }
        
        
    }
    
        
}
