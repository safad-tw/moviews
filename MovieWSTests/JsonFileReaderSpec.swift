//
//  JsonFileReaderSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class JsonFileReaderSpec: QuickSpec{
    
    override func spec() {
        
        it("load data from json file"){
            let jsonData = loadDataFromJSON(fileName: "movies")
            expect(jsonData?.count).to(equal(1))
        }
        
        it("should return nil if json file is not present"){
            let jsonData = loadDataFromJSON(fileName: "mov")
            expect(jsonData).to(beNil())
        }
        
    }
    
    
    
}

