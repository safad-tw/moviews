//
//  FilmServiceProtocolMock.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//


@testable import MovieWS

class FilmServiceProtocolMock: FilmServiceProtocol {
    
    var stubs = [String: [Film]]()
    
    func getFilms() -> [Film]{
        return stubs["getFilms"]!
    }
    
    func stub(_ stubName: String, _ returnValue: [Film]) {
        stubs[stubName] = returnValue
    }
    
    
}
