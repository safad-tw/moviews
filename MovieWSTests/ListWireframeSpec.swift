//
//  ListWireframeSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//


import Quick
import Nimble
@testable import MovieWS

class ListWireframeSpec: QuickSpec {
    
    
    override func spec() {
        var listWireframe: ListWireframe!
        let rootWireframeProtocolMock = RootWireframeProtocolMock()
        
        beforeEach {
            listWireframe = ListWireframe()
            listWireframe.rootWireframe = rootWireframeProtocolMock
        }
        
        
        it("Pass right arguements to root wire frame"){
            let window = UIWindow()
            
            listWireframe.presentListInterfaceFromWindow(window)
            
            let passedArgs:[AnyObject] = rootWireframeProtocolMock.arguments(for: "showRootViewController")!
            expect(passedArgs.count).to(equal(2))
            expect(passedArgs[1] as? UIWindow).to(equal(window))
        }
        
        
        it("Instantiate move list view controller from storyboard"){
            let movieListVC = listWireframe.movieListViewControllerFromStoryboard()
            expect(movieListVC).notTo(beNil())
        }
        
        
        it("Instantiate move detail view controller from storyboard"){
            let movieDetailVC = listWireframe.movieDetailViewControllerFromStoryboard()
            expect(movieDetailVC).notTo(beNil())
        }
        
        it("Go to movie detail screen"){
            listWireframe.goToMovieDetail(with: Film())
        }

        
    }
}
