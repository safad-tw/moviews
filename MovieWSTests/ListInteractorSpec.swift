//
//  ListInteractorSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class ListInteractorSpec: QuickSpec {
    
    override func spec() {
        
        it("should fetch all films from service"){
            
            //Mock
            let filmServiceProtocolMock  = FilmServiceProtocolMock()
            let film1 = Film()
            filmServiceProtocolMock.stub("getFilms", [film1])
            
            let listInteractor = ListInteractor()
            
            listInteractor.service = filmServiceProtocolMock
            expect(listInteractor.getAllFilms().count).to(equal(1))

        }
    }
    
    
    
}
