//
//  DirectorSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS


class DirectorSpec: QuickSpec {
    
    
    override func spec() {
        
        it("should initialze with json data"){
            
            let actorData:[String:AnyObject] = [
                "name" : "Bryan Cranston" as AnyObject,
                "biography" : "Bryan Lee Cranston is an American actor, voice actor, writer and director." as AnyObject
            ]
            
            let director = Director(json:actorData)
            
            expect(director).notTo(beNil())
            expect(director.name).to(equal("Bryan Cranston"))

        }
    }

    
        
}
