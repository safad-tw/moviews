//
//  ListWireframeProtocolMock.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//


@testable import MovieWS

class ListWireframeProtocolMock:ListWireframeProtocol{
    
    var args = [String: Film]()
    
    func arguments(for methodName: String) -> Film? {
        return args[methodName]
    }
    
    
    func goToMovieDetail(with film:Film){
        args["goToMovieDetail"] = film
    }
}
