//
//  RootWireframeSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class RootWireframeSpec: QuickSpec {
    
    override func spec() {
        
        
        it("should set view controllers to window root view controller"){
            
            let rootWireframe = RootWireframe()
            let navigationVC  = UINavigationController()
            let window = UIWindow()
            window.rootViewController = navigationVC
            let rootVC = UIViewController()
            
            rootWireframe.showRootViewController(rootVC, inWindow: window)
            
            expect(navigationVC.viewControllers).notTo(beNil())
            expect(navigationVC.viewControllers.count).to(equal(1))
            expect(navigationVC.viewControllers[0]).to(equal(rootVC))

        }
        
    }
    
    
       
}
