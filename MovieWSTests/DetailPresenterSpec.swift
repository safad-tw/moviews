//
//  DetailPresenterSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class DetailPresenterSpec: QuickSpec {
    
    override func spec() {
        
        var detailPresenter:DetailPresenter!
        
        beforeEach {
            let filmData = loadDataFromJSON(fileName: "movies")
            let film = Film(json:filmData?[0] as! [String : AnyObject])
            
            detailPresenter = DetailPresenter()
            detailPresenter.selectedFilm = film
            
        }
        
        
        it("Should return director name"){
            expect(detailPresenter.getDirectorName()).to(equal("Ben Affleck"))
        }
        
        it("Should return actors names"){
            expect(detailPresenter.getActorsName()).to(equal("Bryan Cranston"))
        }
        
        it("Should return actors  screen names"){
            expect(detailPresenter.getActorsScreenName()).to(equal("Jack Donnell"))
        }

        
    }
    
        
}
