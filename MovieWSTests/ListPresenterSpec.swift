//
//  ListPresenterSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS

class ListPresenterSpec: QuickSpec {
    
    override func spec() {
        
        var listPresenter:ListPresenter!
        var listInteractorProtocolMock:ListInteractorProtocolMock!
        var listWireframeProtocolMock:ListWireframeProtocolMock!
        var film:Film!
        
        beforeEach {
            film = Film()
            listInteractorProtocolMock = ListInteractorProtocolMock()
            listInteractorProtocolMock.stub("getAllFilms", [film])
            listWireframeProtocolMock = ListWireframeProtocolMock()
            listPresenter = ListPresenter(listInteractorProtocolMock,wireFrame: listWireframeProtocolMock)
        }
        
        it("should return films count"){
            expect(listPresenter.numberOfFilms()).to(equal(1))
        }
        
        it("should return film object for index"){
            expect(listPresenter.filmAt(0)) === film
        }
        
        it("should call wireframe detail page method with right args"){
            listPresenter.didSelectFilmAt(0)
            expect(listWireframeProtocolMock.arguments(for: "goToMovieDetail")) === film
        }

    }
}
