//
//  ListInteractorProtocolMock.swift
//  MovieWS
//
//  Created by safadmoh on 1/15/17.
//  Copyright © 2017 Safad. All rights reserved.
//

@testable import MovieWS


class ListInteractorProtocolMock:ListInteractorProtocol{
    
    var stubs = [String: [Film]]()
    var service: FilmServiceProtocol!
    
    func getAllFilms() -> [Film]{
        return stubs["getAllFilms"]!
    }
    
    func stub(_ stubName: String, _ returnValue: [Film]) {
        stubs[stubName] = returnValue
    }
    
}
