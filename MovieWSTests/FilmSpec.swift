//
//  FilmSpec.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Quick
import Nimble
@testable import MovieWS


class FilmSpec: QuickSpec {
    
    override func spec() {
        
        it("should initialze with json data"){
            
            let filmData = loadDataFromJSON(fileName: "movies")
            let film = Film(json:filmData?[0] as! [String : AnyObject])
            
            expect(film).notTo(beNil())
            expect(film.name).to(equal("Argo"))
            expect(film.actors.count).to(equal(1))
            expect(film.director).notTo(beNil())
            expect(film.languages?.count).to(equal(2))
            expect(film.rating).to(equal(7.8))

        }
    }
    
    
       
}
