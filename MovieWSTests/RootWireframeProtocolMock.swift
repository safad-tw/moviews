//
//  File.swift
//  MovieWS
//
//  Created by safadmoh on 1/14/17.
//  Copyright © 2017 Safad. All rights reserved.
//

import Foundation
import UIKit
@testable import MovieWS

class RootWireframeProtocolMock:RootWireframeProtocol{
    
    var args = [String: [AnyObject]]()
    
    func arguments(for methodName: String) -> [AnyObject]? {
        return args[methodName]
    }
    
    func showRootViewController(_ viewController: UIViewController, inWindow: UIWindow){
        args["showRootViewController"] = [viewController,inWindow]
    }
    
}
